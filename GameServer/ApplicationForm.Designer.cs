﻿
namespace GameServer
{
    partial class ApplicationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Status = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.playerMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.playerInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblTOD = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblLoginPort = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblLoginServer = new System.Windows.Forms.Label();
            this.lblPort = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblExternalAddress = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblTotalMaps = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblCurrentEvent = new System.Windows.Forms.Label();
            this.lblUpTime = new System.Windows.Forms.Label();
            this.lblTotalNpcs = new System.Windows.Forms.Label();
            this.lblPlayersOnline = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lstGameServers = new System.Windows.Forms.ListBox();
            this.btnUpdateNews = new System.Windows.Forms.Button();
            this.txtNews = new System.Windows.Forms.TextBox();
            this.Chat = new System.Windows.Forms.TabPage();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btnChatSend = new System.Windows.Forms.Button();
            this.txtChatName = new System.Windows.Forms.TextBox();
            this.txtChatText = new System.Windows.Forms.TextBox();
            this.lstChat = new System.Windows.Forms.ListBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tabControl1.SuspendLayout();
            this.Status.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.playerMenu.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.Chat.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Status);
            this.tabControl1.Controls.Add(this.Chat);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 30);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1043, 821);
            this.tabControl1.TabIndex = 2;
            // 
            // Status
            // 
            this.Status.Controls.Add(this.groupBox2);
            this.Status.Controls.Add(this.groupBox1);
            this.Status.Controls.Add(this.btnUpdateNews);
            this.Status.Controls.Add(this.txtNews);
            this.Status.Location = new System.Drawing.Point(4, 29);
            this.Status.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(1035, 788);
            this.Status.TabIndex = 0;
            this.Status.Text = "Status";
            this.Status.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridView1);
            this.groupBox2.Location = new System.Drawing.Point(269, 248);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Size = new System.Drawing.Size(755, 528);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Players";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.ContextMenuStrip = this.playerMenu;
            this.dataGridView1.Location = new System.Drawing.Point(8, 29);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.ShowEditingIcon = false;
            this.dataGridView1.ShowRowErrors = false;
            this.dataGridView1.Size = new System.Drawing.Size(739, 489);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.PlayerSelected);
            // 
            // playerMenu
            // 
            this.playerMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.playerMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.playerInfo});
            this.playerMenu.Name = "playerMenu";
            this.playerMenu.Size = new System.Drawing.Size(145, 28);
            // 
            // playerInfo
            // 
            this.playerInfo.Name = "playerInfo";
            this.playerInfo.Size = new System.Drawing.Size(144, 24);
            this.playerInfo.Text = "Show Info";
            this.playerInfo.Click += new System.EventHandler(this.ShowPlayerInfo);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblStatus);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.lblTOD);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.lblLoginPort);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.lblLoginServer);
            this.groupBox1.Controls.Add(this.lblPort);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.lblExternalAddress);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.lblTotalMaps);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.lblCurrentEvent);
            this.groupBox1.Controls.Add(this.lblUpTime);
            this.groupBox1.Controls.Add(this.lblTotalNpcs);
            this.groupBox1.Controls.Add(this.lblPlayersOnline);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.lstGameServers);
            this.groupBox1.Location = new System.Drawing.Point(269, 5);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(755, 234);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Game Servers";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(300, 29);
            this.lblStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(70, 20);
            this.lblStatus.TabIndex = 24;
            this.lblStatus.Text = "Starting...";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(239, 29);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 20);
            this.label11.TabIndex = 23;
            this.label11.Text = "Status:";
            // 
            // lblTOD
            // 
            this.lblTOD.AutoSize = true;
            this.lblTOD.Location = new System.Drawing.Point(300, 165);
            this.lblTOD.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTOD.Name = "lblTOD";
            this.lblTOD.Size = new System.Drawing.Size(70, 20);
            this.lblTOD.TabIndex = 22;
            this.lblTOD.Text = "Unknown";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(203, 165);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(93, 20);
            this.label10.TabIndex = 21;
            this.label10.Text = "Time of Day:";
            // 
            // lblLoginPort
            // 
            this.lblLoginPort.AutoSize = true;
            this.lblLoginPort.Location = new System.Drawing.Point(588, 97);
            this.lblLoginPort.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLoginPort.Name = "lblLoginPort";
            this.lblLoginPort.Size = new System.Drawing.Size(70, 20);
            this.lblLoginPort.TabIndex = 20;
            this.lblLoginPort.Text = "Unknown";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(503, 97);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 20);
            this.label9.TabIndex = 19;
            this.label9.Text = "Login Port:";
            // 
            // lblLoginServer
            // 
            this.lblLoginServer.AutoSize = true;
            this.lblLoginServer.Location = new System.Drawing.Point(588, 63);
            this.lblLoginServer.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLoginServer.Name = "lblLoginServer";
            this.lblLoginServer.Size = new System.Drawing.Size(70, 20);
            this.lblLoginServer.TabIndex = 18;
            this.lblLoginServer.Text = "Unknown";
            // 
            // lblPort
            // 
            this.lblPort.AutoSize = true;
            this.lblPort.Location = new System.Drawing.Point(588, 165);
            this.lblPort.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPort.Name = "lblPort";
            this.lblPort.Size = new System.Drawing.Size(70, 20);
            this.lblPort.TabIndex = 17;
            this.lblPort.Text = "Unknown";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(477, 63);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(106, 20);
            this.label7.TabIndex = 16;
            this.label7.Text = "Login Address:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(487, 165);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(95, 20);
            this.label8.TabIndex = 15;
            this.label8.Text = "External Port:";
            // 
            // lblExternalAddress
            // 
            this.lblExternalAddress.AutoSize = true;
            this.lblExternalAddress.Location = new System.Drawing.Point(588, 131);
            this.lblExternalAddress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblExternalAddress.Name = "lblExternalAddress";
            this.lblExternalAddress.Size = new System.Drawing.Size(70, 20);
            this.lblExternalAddress.TabIndex = 14;
            this.lblExternalAddress.Text = "Unknown";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(461, 131);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(122, 20);
            this.label5.TabIndex = 13;
            this.label5.Text = "External Address:";
            // 
            // lblTotalMaps
            // 
            this.lblTotalMaps.AutoSize = true;
            this.lblTotalMaps.Location = new System.Drawing.Point(300, 131);
            this.lblTotalMaps.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTotalMaps.Name = "lblTotalMaps";
            this.lblTotalMaps.Size = new System.Drawing.Size(17, 20);
            this.lblTotalMaps.TabIndex = 12;
            this.lblTotalMaps.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(208, 131);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 20);
            this.label6.TabIndex = 11;
            this.label6.Text = "Total Maps:";
            // 
            // lblCurrentEvent
            // 
            this.lblCurrentEvent.AutoSize = true;
            this.lblCurrentEvent.Location = new System.Drawing.Point(588, 29);
            this.lblCurrentEvent.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCurrentEvent.Name = "lblCurrentEvent";
            this.lblCurrentEvent.Size = new System.Drawing.Size(45, 20);
            this.lblCurrentEvent.TabIndex = 10;
            this.lblCurrentEvent.Text = "None";
            // 
            // lblUpTime
            // 
            this.lblUpTime.AutoSize = true;
            this.lblUpTime.Location = new System.Drawing.Point(300, 198);
            this.lblUpTime.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUpTime.Name = "lblUpTime";
            this.lblUpTime.Size = new System.Drawing.Size(17, 20);
            this.lblUpTime.TabIndex = 9;
            this.lblUpTime.Text = "0";
            // 
            // lblTotalNpcs
            // 
            this.lblTotalNpcs.AutoSize = true;
            this.lblTotalNpcs.Location = new System.Drawing.Point(300, 97);
            this.lblTotalNpcs.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTotalNpcs.Name = "lblTotalNpcs";
            this.lblTotalNpcs.Size = new System.Drawing.Size(17, 20);
            this.lblTotalNpcs.TabIndex = 8;
            this.lblTotalNpcs.Text = "0";
            // 
            // lblPlayersOnline
            // 
            this.lblPlayersOnline.AutoSize = true;
            this.lblPlayersOnline.Location = new System.Drawing.Point(300, 63);
            this.lblPlayersOnline.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPlayersOnline.Name = "lblPlayersOnline";
            this.lblPlayersOnline.Size = new System.Drawing.Size(17, 20);
            this.lblPlayersOnline.TabIndex = 7;
            this.lblPlayersOnline.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(225, 198);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "Up Time:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(439, 29);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(144, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "Current World Event:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(209, 97);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Total Npcs:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(189, 63);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Players Online:";
            // 
            // lstGameServers
            // 
            this.lstGameServers.FormattingEnabled = true;
            this.lstGameServers.ItemHeight = 20;
            this.lstGameServers.Location = new System.Drawing.Point(8, 29);
            this.lstGameServers.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lstGameServers.Name = "lstGameServers";
            this.lstGameServers.Size = new System.Drawing.Size(159, 184);
            this.lstGameServers.TabIndex = 2;
            this.lstGameServers.SelectedIndexChanged += new System.EventHandler(this.UpdateGameServerInfo);
            // 
            // btnUpdateNews
            // 
            this.btnUpdateNews.Enabled = false;
            this.btnUpdateNews.Location = new System.Drawing.Point(4, 740);
            this.btnUpdateNews.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnUpdateNews.Name = "btnUpdateNews";
            this.btnUpdateNews.Size = new System.Drawing.Size(257, 35);
            this.btnUpdateNews.TabIndex = 1;
            this.btnUpdateNews.Text = "Update News";
            this.btnUpdateNews.UseVisualStyleBackColor = true;
            this.btnUpdateNews.Click += new System.EventHandler(this.UpdateNews);
            // 
            // txtNews
            // 
            this.txtNews.AcceptsReturn = true;
            this.txtNews.Location = new System.Drawing.Point(4, 5);
            this.txtNews.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtNews.Multiline = true;
            this.txtNews.Name = "txtNews";
            this.txtNews.Size = new System.Drawing.Size(256, 724);
            this.txtNews.TabIndex = 0;
            this.txtNews.TextChanged += new System.EventHandler(this.OnNewsModified);
            // 
            // Chat
            // 
            this.Chat.Controls.Add(this.label14);
            this.Chat.Controls.Add(this.label12);
            this.Chat.Controls.Add(this.btnChatSend);
            this.Chat.Controls.Add(this.txtChatName);
            this.Chat.Controls.Add(this.txtChatText);
            this.Chat.Controls.Add(this.lstChat);
            this.Chat.Location = new System.Drawing.Point(4, 29);
            this.Chat.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Chat.Name = "Chat";
            this.Chat.Size = new System.Drawing.Size(1035, 788);
            this.Chat.TabIndex = 1;
            this.Chat.Text = "Chat";
            this.Chat.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(288, 749);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(70, 20);
            this.label14.TabIndex = 6;
            this.label14.Text = "Message:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(11, 749);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 20);
            this.label12.TabIndex = 4;
            this.label12.Text = "Name:";
            // 
            // btnChatSend
            // 
            this.btnChatSend.Location = new System.Drawing.Point(937, 742);
            this.btnChatSend.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnChatSend.Name = "btnChatSend";
            this.btnChatSend.Size = new System.Drawing.Size(87, 35);
            this.btnChatSend.TabIndex = 3;
            this.btnChatSend.Text = "Send";
            this.btnChatSend.UseVisualStyleBackColor = true;
            this.btnChatSend.Click += new System.EventHandler(this.SendChat);
            // 
            // txtChatName
            // 
            this.txtChatName.Location = new System.Drawing.Point(69, 745);
            this.txtChatName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtChatName.MaxLength = 10;
            this.txtChatName.Name = "txtChatName";
            this.txtChatName.Size = new System.Drawing.Size(176, 27);
            this.txtChatName.TabIndex = 2;
            this.txtChatName.Text = "Admin[GM]";
            // 
            // txtChatText
            // 
            this.txtChatText.Location = new System.Drawing.Point(367, 745);
            this.txtChatText.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtChatText.MaxLength = 70;
            this.txtChatText.Name = "txtChatText";
            this.txtChatText.Size = new System.Drawing.Size(561, 27);
            this.txtChatText.TabIndex = 1;
            this.txtChatText.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SendChatKeyDown);
            // 
            // lstChat
            // 
            this.lstChat.Dock = System.Windows.Forms.DockStyle.Top;
            this.lstChat.FormattingEnabled = true;
            this.lstChat.ItemHeight = 20;
            this.lstChat.Location = new System.Drawing.Point(0, 0);
            this.lstChat.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lstChat.Name = "lstChat";
            this.lstChat.Size = new System.Drawing.Size(1035, 704);
            this.lstChat.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.optionsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 3, 0, 3);
            this.menuStrip1.Size = new System.Drawing.Size(1043, 30);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(46, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(116, 26);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.Exit);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(75, 24);
            this.optionsToolStripMenuItem.Text = "Options";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 5000;
            this.timer1.Tick += new System.EventHandler(this.UpdateGameServerInfo);
            // 
            // ApplicationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1043, 851);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximumSize = new System.Drawing.Size(1061, 898);
            this.Name = "ApplicationForm";
            this.Text = "Helbreath Game Server Manager by Vamp";
            this.tabControl1.ResumeLayout(false);
            this.Status.ResumeLayout(false);
            this.Status.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.playerMenu.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.Chat.ResumeLayout(false);
            this.Chat.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage Status;
        private System.Windows.Forms.Button btnUpdateNews;
        private System.Windows.Forms.TextBox txtNews;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lstGameServers;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblPlayersOnline;
        private System.Windows.Forms.Label lblCurrentEvent;
        private System.Windows.Forms.Label lblUpTime;
        private System.Windows.Forms.Label lblTotalNpcs;
        private System.Windows.Forms.Label lblTotalMaps;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.Label lblLoginServer;
        private System.Windows.Forms.Label lblPort;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblExternalAddress;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblLoginPort;
        private System.Windows.Forms.Label lblTOD;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ContextMenuStrip playerMenu;
        private System.Windows.Forms.ToolStripMenuItem playerInfo;
        private System.Windows.Forms.TabPage Chat;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnChatSend;
        private System.Windows.Forms.TextBox txtChatName;
        private System.Windows.Forms.TextBox txtChatText;
        private System.Windows.Forms.ListBox lstChat;

    }
}

