﻿using Helbreath.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace GameServer
{
    public class Scheduler
    {
        public LogHandler MessageLogged;

        private List<ScheduledTask> tasks;
        private Timer timer;

        private World world;


        public Scheduler()
        {
            tasks = new List<ScheduledTask>();
        }

        public void Start(World world)
        {
            this.world = world;

            timer = new Timer(new TimerCallback(Check));
            timer.Change(0, 6000); // CHECK every 1 min
        }

        public void AddTask(ScheduledTask task)
        {
            tasks.Add(task);
        }

        public void RemoveTask(ScheduledTask task)
        {
            tasks.Remove(task);
        }

        public void Check(object timerObj)
        {
            foreach (ScheduledTask task in tasks)
                if (DateTime.Now.DayOfWeek == task.Day)
                    if (!task.IsRunning)
                    {
                        if (task.NextRun.Hour == DateTime.Now.Hour &&
                            task.NextRun.Minute == DateTime.Now.Minute) Run(task);
                    }
                    else
                    {
                        if (task.ScheduleEnd.Hour == DateTime.Now.Hour &&
                            task.ScheduleEnd.Minute == DateTime.Now.Minute) End(task);
                    }
        }

        public void Run(ScheduledTask task)
        {
            task.IsRunning = true;
            task.NextRun = task.Schedule.AddDays(7);
            task.LastRun = DateTime.Now;

            switch (task.Type)
            {
                case TaskType.WorldEvent:
                    WorldEventType typeParser;
                    if (Enum.TryParse<WorldEventType>(task.Value, out typeParser))
                        world.StartWorldEvent(typeParser);
                    else LogMessage(string.Format("Unable to parse scheduled event [{0}] of type [{1}] when attempting to Run.", task.Value, task.Type.ToString()));
                    break;
            }

            LogMessage(string.Format("Task run [{0} ({3})] at [{1}]. Next run at [{2}]", task.Type.ToString(), task.LastRun.ToString(), task.NextRun.ToString(), task.Value));
        }

        public void End(ScheduledTask task)
        {
            switch (task.Type)
            {
                case TaskType.WorldEvent:
                    WorldEventType typeParser;
                    if (Enum.TryParse<WorldEventType>(task.Value, out typeParser))
                        world.EndWorldEvent(OwnerSide.Neutral);
                    else LogMessage(string.Format("Unable to parse scheduled event [{0}] of type [{1}] when attempting to End.", task.Value, task.Type.ToString()));
                    break;
            }

            LogMessage(string.Format("Task ended [{0} ({3})] at [{1}]. Next run at [{2}]", task.Type.ToString(), task.LastRun.ToString(), task.NextRun.ToString(), task.Value));
        }

        private void LogMessage(string message)
        {
            if (MessageLogged != null) MessageLogged(message,LogType.Events);
        }

        public List<ScheduledTask> Tasks { get { return tasks; } }
    }

    public class ScheduledTask
    {
        private String value;
        private TaskType type;
        private DateTime schedule;
        private DateTime scheduleEnd;
        private DateTime lastRun;
        private DateTime nextRun;
        private DayOfWeek day;
        private bool isRunning;

        public ScheduledTask(String value, TaskType type, DayOfWeek day, DateTime schedule)
        {
            this.value = value;
            this.type = type;
            this.day = day;
            this.schedule = nextRun = schedule;
            this.scheduleEnd = DateTime.Now.AddYears(1);
        }

        public ScheduledTask(String value, TaskType type, DayOfWeek day, DateTime schedule, DateTime scheduleEnd)
        {
            this.value = value;
            this.type = type;
            this.day = day;
            this.schedule = nextRun = schedule;
            this.scheduleEnd = scheduleEnd;
        }

        public static ScheduledTask ParseXml(string value, TaskType type, XmlReader r)
        {
            if (r["StartTime"] == null) return null;

            int startHour, startMinute, endHour = 1, endMinute = 1;

            Int32.TryParse(r["StartTime"].Split(':')[0], out startHour);
            Int32.TryParse(r["StartTime"].Split(':')[1], out startMinute);

            if (r["EndTime"] != null)
            {
                Int32.TryParse(r["EndTime"].Split(':')[0], out endHour);
                Int32.TryParse(r["EndTime"].Split(':')[1], out endMinute);
            }

            DayOfWeek day = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), r["Day"]);


            ScheduledTask task;

            if (r["EndTime"] == null)
                task = new ScheduledTask(value, type, day, new DateTime(1, 1, 1, startHour, startMinute, 0));
            else task = new ScheduledTask(value, type, day, new DateTime(1, 1, 1, startHour, startMinute, 0), new DateTime(1, 1, 1, endHour, endMinute, 0));

            return task;
        }

        public String Value { get { return value; } }
        public TaskType Type { get { return type; } }
        public DateTime Schedule { get { return schedule; } }
        public DateTime ScheduleEnd { get { return scheduleEnd; } }
        public DateTime LastRun { get { return lastRun; } set { lastRun = value; } }
        public DateTime NextRun { get { return nextRun; } set { nextRun = value; } }
        public DayOfWeek Day { get { return day; } }
        public Boolean IsRunning { get { return isRunning; } set { isRunning = value; } }
    }

    public enum TaskType
    {
        WorldEvent
    }
}
